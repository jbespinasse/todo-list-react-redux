import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import configureMockStore from 'redux-mock-store'

import * as types from '../constants/TodoActionTypes'
import * as actions from './todosAction'
import APIConfig from '../constants/APIConfig'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('actions', () => {
  const mockTodosJSON = [
    { id: 1, text: 'todo text 1', completed: false },
    { id: 2, text: 'todo text 2', completed: true },
    { id: 3, text: 'todo text 3', completed: false }
  ]

  const mockError = { error: true, message: "An error occurred." }

  const initialeState = {
    loading: false,
    missingApi: false,
    todos: [],
    error: null
  }

  let store = mockStore({ todos: initialeState })

  describe('Test todos Action Type', () => {
    
    afterEach(() => fetchMock.restore())

    it('should create the expected actions for fetchTodos', () => {

      fetchMock.get(`${APIConfig.API_URI}todos/all`, {
        body: mockTodosJSON,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.REQUEST_TODOS,
          loading: true
        }, {
          type: types.REQUEST_TODOS_SUCCESS,
          loading: false,
          todos: mockTodosJSON
        }]
      ]

      return store.dispatch(actions.fetchTodos())
        .then(() => expect(store.getActions()).toMatchObject(expectedActions))
    })

    it('should create the expected actions with error for fetchTodos', () => {

      fetchMock.get(`${APIConfig.API_URI}todos/all`, {
        throws: mockError,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.REQUEST_TODOS,
          missingApi: false,
          loading: true
        }, {
          type: types.REQUEST_TODOS_ERROR, 
          loading: false,
          missingApi: true,
          error: mockError
        }]
      ]

      return store.dispatch(actions.fetchTodos())
        .then(() => expect(store.getActions()).toEqual(expectedActions))
    })

    it('should create the expected actions for fetchAddTodo', () => {

      const newTodo = { todo: { text: "New todo" } }

      fetchMock.post(`${APIConfig.API_URI}todos/create`, {
        body: newTodo,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.ADD_TODO,
          loading: true
        }, {
          type: types.ADD_TODO_SUCCESS,
          loading: false,
          todo: newTodo
        }]
      ]

      return store.dispatch(actions.fetchAddTodo(newTodo))
        .then(() => expect(store.getActions()).toMatchObject(expectedActions))
    })

    it('should create the expected actions with error for fetchAddTodo', () => {

      const newTodo = { todo: { text: "New todo" } }

      fetchMock.post(`${APIConfig.API_URI}todos/create`, {
        throws: mockError,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.ADD_TODO,
          loading: true
        }, {
          type: types.ADD_TODO_ERROR,
          loading: false,
          error: mockError
        }]
      ]

      return store.dispatch(actions.fetchAddTodo(newTodo))
        .then(() => expect(store.getActions()).toEqual(expectedActions))
    })

    it('should create the expected actions for fetchUpdateTodo', () => {

      const updTodo = { todo: { text: "Updated todo" } }

      fetchMock.post(`${APIConfig.API_URI}todos/update`, {
        body: updTodo,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.UPDATE_TODO,
          loading: true
        }, {
          type: types.UPDATE_TODO_SUCCESS,
          loading: false,
          todo: updTodo
        }]
      ]

      return store.dispatch(actions.fetchUpdateTodo(updTodo))
        .then(() => expect(store.getActions()).toMatchObject(expectedActions))
    })

    it('should create the expected actions with error for fetchUpdateTodo', () => {

      const updTodo = { todo: { text: "Updated todo" } }

      fetchMock.post(`${APIConfig.API_URI}todos/update`, {
        throws: mockError,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.UPDATE_TODO,
          loading: true
        }, {
          type: types.UPDATE_TODO_ERROR,
          loading: false,
          error: mockError
        }]
      ]

      return store.dispatch(actions.fetchUpdateTodo(updTodo))
        .then(() => expect(store.getActions()).toEqual(expectedActions))
    })

    it('should create the expected actions for fetchDeleteTodo', () => {

      const id = { id: 1 }

      fetchMock.post(`${APIConfig.API_URI}todos/delete`, {
        body: id,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.DELETE_TODO,
          loading: true
        }, {
          type: types.DELETE_TODO_SUCCESS,
          loading: false,
          ...id
        }]
      ]

      return store.dispatch(actions.fetchDeleteTodo(id))
        .then(() => expect(store.getActions()).toMatchObject(expectedActions))
    })

    it('should create the expected actions with error for fetchDeleteTodo', () => {

      const id = { id: 1 }

      fetchMock.post(`${APIConfig.API_URI}todos/delete`, {
        throws: mockError,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.DELETE_TODO,
          loading: true
        }, {
          type: types.DELETE_TODO_ERROR,
          loading: false,
          error: mockError
        }]
      ]

      return store.dispatch(actions.fetchDeleteTodo(id))
        .then(() => expect(store.getActions()).toEqual(expectedActions))
    })

    it('should create the expected actions for fetchCompleteAllTodos', () => {

      fetchMock.post(`${APIConfig.API_URI}todos/update/all`, {
        body: mockTodosJSON,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.REQUEST_COMPLETE_ALL_TODOS,
          loading: true
        }, {
          type: types.REQUEST_COMPLETE_ALL_TODOS_SUCCESS,
          loading: false,
          todos: mockTodosJSON
        }]
      ]

      return store.dispatch(actions.fetchCompleteAllTodos())
        .then(() => expect(store.getActions()).toMatchObject(expectedActions))
    })

    it('should create the expected actions with error for fetchCompleteAllTodos', () => {

      const id = { id: 1 }

      fetchMock.post(`${APIConfig.API_URI}todos/update/all`, {
        throws: mockError,
        headers: APIConfig.headers
      })

      const expectedActions = [
        ...store.getActions(),
        ...[{
          type: types.REQUEST_COMPLETE_ALL_TODOS,
          loading: true
        }, {
          type: types.REQUEST_COMPLETE_ALL_TODOS_ERROR,
          loading: false,
          error: mockError
        }]
      ]

      return store.dispatch(actions.fetchCompleteAllTodos())
        .then(() => expect(store.getActions()).toEqual(expectedActions))
    })

  })
})