import * as types from '../constants/TodoActionTypes'
import APIConfig from '../constants/APIConfig'

/**
 * GET ALL TODOS
 */
export const requestTodos = () => ({
	type: types.REQUEST_TODOS,
	loading: true,
	missingApi: false
})
export const requestTodosSuccess = (todos) => ({
	type: types.REQUEST_TODOS_SUCCESS,
	loading: false,
	todos
})
export const requestTodosError = (error) => ({
	type: types.REQUEST_TODOS_ERROR,
	loading: false,
	missingApi: true,
	error
})
export const fetchTodos = () => (dispatch, getState) => {
	dispatch(requestTodos())

	return fetch(
		`${APIConfig.API_URI}todos/all`, {
			method: 'GET',
			headers: APIConfig.HEADERS,
		})
		.then((response) => {
			if (response.error) {
				throw new Error(response.message || 'An error occurred.')
			}
			return response.json()
		})
		.then((todos) => dispatch(requestTodosSuccess(todos)))
		.catch((error) => dispatch(requestTodosError(error)))
}

/**
 * ADD A TODO
 */
export const requestAddTodo = () => ({
	type: types.ADD_TODO,
	loading: true
})
export const requestAddTodoSuccess = (todo) => ({
	type: types.ADD_TODO_SUCCESS,
	loading: false,
	todo
})
export const requestAddTodoError = (error) => ({
	type: types.ADD_TODO_ERROR,
	loading: false,
	error
})
export const fetchAddTodo = (todo) => (dispatch) => {
	dispatch(requestAddTodo())

	return fetch(
		`${APIConfig.API_URI}todos/create`, {
			method: 'POST',
			body: JSON.stringify({ todo: todo }),
			headers: APIConfig.HEADERS,
		})
		.then((response) => {
			if (response.error) {
				throw new Error('An error occurred.')
			}
			return response.json()
		})
		.then((todo) => dispatch(requestAddTodoSuccess(todo)))
		.catch((error) => dispatch(requestAddTodoError(error)))
}

/**
 * UPDATE A TODO
 */
export const updateTodo = () => ({
	type: types.UPDATE_TODO,
	loading: true,
})
export const updateTodoSuccess = (todo) => ({
	type: types.UPDATE_TODO_SUCCESS,
	loading: false,
	todo
})
export const updateTodoError = (error) => ({
	type: types.UPDATE_TODO_ERROR,
	loading: false,
	error
})
export const fetchUpdateTodo = (todo) => (dispatch) => {
	dispatch(updateTodo())

	return fetch(
		`${APIConfig.API_URI}todos/update`, {
			method: 'POST',
			body: JSON.stringify({ todo: todo }),
			headers: APIConfig.HEADERS,
		})
		.then((response) => {
			if (response.error) {
				throw new Error('An error occurred.')
			}
			return response.json()
		})
		.then((todo) => dispatch(updateTodoSuccess(todo)))
		.catch((error) => dispatch(updateTodoError(error)))
}

/**
 * DELETE A TODO
 */
export const deleteTodo = (id) => ({
	type: types.DELETE_TODO,
	loading: true,
	id
})
export const deleteTodoSuccess = (id) => ({
	type: types.DELETE_TODO_SUCCESS,
	loading: false,
	id
})
export const deleteTodoError = (error) => ({
	type: types.DELETE_TODO_ERROR,
	loading: false,
	error
})
export const fetchDeleteTodo = (id) => (dispatch) => {
	dispatch(deleteTodo())

	return fetch(
		`${APIConfig.API_URI}todos/delete`, {
			method: 'POST',
			body: JSON.stringify({ id: id }),
			headers: APIConfig.HEADERS,
		})
		.then((response) => {
			if (response.error) {
				throw new Error('An error occurred.')
			}
			return response.json()
		})
		.then((response) => dispatch(deleteTodoSuccess(response.id)))
		.catch((error) => dispatch(deleteTodoError(error)))
}

/**
 * COMPLETE ALL TODOS
 */
export const requestCompleteAllTodos = () => ({
	type: types.REQUEST_COMPLETE_ALL_TODOS,
	loading: true
})
export const requestCompleteAllTodosSuccess = (todos) => ({
	type: types.REQUEST_COMPLETE_ALL_TODOS_SUCCESS,
	loading: false,
	todos
})
export const requestCompleteAllTodosError = (error) => ({
	type: types.REQUEST_COMPLETE_ALL_TODOS_ERROR,
	loading: false,
	error
})
export const fetchCompleteAllTodos = () => (dispatch, getState) => {
	dispatch(requestCompleteAllTodos())

	const state = getState()
	const todos = state.todos.todos
		.filter(todo => !todo.completed)
		.map(todo => { return { ...todo, completed: !todo.completed } });

	return fetch(
		`${APIConfig.API_URI}todos/update/all`, {
			method: 'POST',
			body: JSON.stringify({ todos: todos }),
			headers: APIConfig.HEADERS,
		})
		.then((response) => {
			if (response.error) {
				throw new Error('An error occurred.')
			}
			return response.json()
		})
		.then((todos) => dispatch(requestCompleteAllTodosSuccess(todos)))
		.catch((error) => dispatch(requestCompleteAllTodosError(error)))
}

export const setVisibilityFilter = (filter) => ({
	type: types.SET_VISIBILITY_FILTER,
	filter
})
export const todoIdChanged = (id) => ({
	type: types.TODO_ID_CHANGED,
	id
})