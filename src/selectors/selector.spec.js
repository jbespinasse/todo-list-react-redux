import {
  getVisibleTodos,
  getCompletedTodoCount,
  getUncompletedTodoCount
} from './index'

import * as filters from '../constants/TodoFilters'

const mockTodosJSON = [
  { _id: 1, text: 'todo text 1', completed: false },
  { _id: 2, text: 'todo text 2', completed: true },
  { _id: 3, text: 'todo text 3', completed: false }
]

describe('Filter Selectors', () => {

  describe('getVisibleTodos', () => {

    it('should return all todos', () => {

      const selected = getVisibleTodos.resultFunc(
        filters.SHOW_ALL,
        { todos: mockTodosJSON }
      )

      expect(selected).toEqual(mockTodosJSON)
    })

    it('should return all completed todos', () => {

      const selected = getVisibleTodos.resultFunc(
        filters.SHOW_COMPLETED,
        { todos: mockTodosJSON }
      )

      expect(selected).toEqual([
        { _id: 2, text: 'todo text 2', completed: true }
      ])
    })

    it('should return all active todos', () => {

      const selected = getVisibleTodos.resultFunc(
        filters.SHOW_ACTIVE,
        { todos: mockTodosJSON }
      )
      
      expect(selected).toEqual([
        { _id: 1, text: 'todo text 1', completed: false },
        { _id: 3, text: 'todo text 3', completed: false }
      ])
    })

  })

  describe('get completed / uncompleted todos count', () => {

    it('should return completed todos count', () => {

      const selected = getCompletedTodoCount.resultFunc(
        { todos: mockTodosJSON }
      )

      expect(selected).toEqual(1)
    })

    it('should return uncompleted todos count', () => {

      const selected = getUncompletedTodoCount.resultFunc(
        { todos: mockTodosJSON }
      )

      expect(selected).toEqual(2)
    })
  })
})