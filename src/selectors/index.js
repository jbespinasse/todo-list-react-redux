import { createSelector } from 'reselect'

import {
	SHOW_ALL,
	SHOW_COMPLETED,
	SHOW_ACTIVE,
} from '../constants/TodoFilters'

const getVisibilityFilter = (state) => state.visibilityFilter
const getTodos = (state) => state.todos

export const getVisibleTodos = createSelector(
	[getVisibilityFilter, getTodos],
	(visibilityFilter, state) => {
		switch (visibilityFilter) {
			case SHOW_ALL:
				return state.todos
			case SHOW_COMPLETED:
				return state.todos.filter((t) => t.completed)
			case SHOW_ACTIVE:
				return state.todos.filter((t) => !t.completed)
			default:
				throw new Error(`Unknown filter: ${visibilityFilter}`)
		}
	}
)

export const getCompletedTodoCount = createSelector(
	[getTodos],
	(state) => state.todos.reduce((count, todo) => (todo.completed ? count + 1 : count), 0)
)

export const getUncompletedTodoCount = createSelector(
	[getTodos],
	(state) => state.todos.reduce((count, todo) => (!todo.completed ? count + 1 : count), 0)
)