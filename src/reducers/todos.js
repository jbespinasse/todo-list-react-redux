import * as TodoActionTypes from '../constants/TodoActionTypes'

export const initialTodosState = ({
  loading: false,
  missingApi: false,
  todos: [],
  error: null
})

const todos = (state = initialTodosState, action) => {
	switch (action.type) {
    /**
     * GET ALL TODOS
     */
    case TodoActionTypes.REQUEST_TODOS:
			return {
        ...state,
        loading: action.loading
      }
    case TodoActionTypes.REQUEST_TODOS_SUCCESS:
      return {
        ...state,
        loading: action.loading,
        todos: action.todos
      }
    case TodoActionTypes.REQUEST_TODOS_ERROR:
      return {
        ...state, 
        loading: action.loading,
        error: action.error,
        missingApi: action.missingApi
      }

    /**
     * ADD A TODO
     */
		case TodoActionTypes.ADD_TODO:
			return {
        ...state,
        loading: action.loading
      }
    case TodoActionTypes.ADD_TODO_SUCCESS:
      return {
        ...state, 
        loading: action.loading, 
        todos: [action.todo].concat(state.todos)
      }
    case TodoActionTypes.ADD_TODO_ERROR:
      return {
        ...state,
        loading: action.loading,
        error: action.error
      }

    /**
     * UPDATE A TODO
     */
    case TodoActionTypes.UPDATE_TODO:
			return {
        ...state,
        loading: action.loading
      }
    case TodoActionTypes.UPDATE_TODO_SUCCESS:
      return {
        ...state,
        loading: action.loading,
        todos: state.todos.map(obj => obj._id === action.todo._id ? { ...obj, ...action.todo } : obj)
      }
    case TodoActionTypes.UPDATE_TODO_ERROR:
      return {
        ...state,
        loading: action.loading,
        error: action.error
      }

    /**
     * DELETE A TODO
     */
    case TodoActionTypes.DELETE_TODO:
      return {
        ...state,
        loading: action.loading
      }
    case TodoActionTypes.DELETE_TODO_SUCCESS:
      return {
        ...state,
        loading: action.loading,
        todos: state.todos.filter(todo => todo._id !== action.id)
      }
    case TodoActionTypes.DELETE_TODO_ERROR:
      return {
        ...state,
        loading: action.loading,
        error: action.error
      }

    /**
     * COMPLETE ALL TODOS
     */
    case TodoActionTypes.REQUEST_COMPLETE_ALL_TODOS:
			return {
        ...state,
        loading: action.loading
      }
    case TodoActionTypes.REQUEST_COMPLETE_ALL_TODOS_SUCCESS:
      return { 
        ...state, 
        loading: action.loading, 
        todos: state.todos.map(obj => action.todos.find(o => o._id === obj._id) || obj)
      }
    case TodoActionTypes.REQUEST_COMPLETE_ALL_TODOS_ERROR:
      return {
        ...state,
        loading: action.loading,
        error: action.error
      }
      
		default:
			return state
	}
}

export default todos