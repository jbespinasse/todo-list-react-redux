import reducer, { initialTodoState } from './todo'
import * as actions from '../actions/todosAction'

describe('todo reducer', () => {

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialTodoState)
  })

  it('should handle TODO_ID_CHANGED', () => {

    expect(
      reducer(undefined, actions.todoIdChanged(3))
    ).toEqual({
      id: 3
    })

  })

})