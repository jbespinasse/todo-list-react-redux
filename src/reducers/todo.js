import * as TodoActionTypes from '../constants/TodoActionTypes'

export const initialTodoState = ({ id: null })

const todo = (state = initialTodoState, action) => {
	switch (action.type) {
    case TodoActionTypes.TODO_ID_CHANGED:
			return { ...state, id: action.id }
		default:
			return state
	}
}

export default todo