import reducer, { initialTodosState } from './todos'
import * as actions from '../actions/todosAction'

const mockTodosJSON = [
  { _id: 1, text: 'todo text 1', completed: false },
  { _id: 2, text: 'todo text 2', completed: true },
  { _id: 3, text: 'todo text 3', completed: false }
]
const mockNewTodo = { _id: 4, text: 'New Todo', completed: false }
const mockError = { error: true, message: "An error occurred." }

describe('todos reducer', () => {

  beforeEach(() => {
    // store = mockStore(initialTodosState)
  })

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialTodosState)
  })

  it('should handle REQUEST_TODOS', () => {

    /**
     * REDUCER REQUEST TODOS
     */
    expect(
      reducer(undefined, actions.requestTodos())
    ).toEqual({
      todos: [],
      loading: true,
      error: null,
      missingApi: false
    })

    expect(
      reducer(undefined, actions.requestTodosSuccess(mockTodosJSON))
    ).toEqual({
      loading: false,
      todos: mockTodosJSON,
      error: null,
      missingApi: false
    })

    expect(
      reducer(undefined, actions.requestTodosError(mockError))
    ).toEqual({
      loading: false,
      error: mockError,
      missingApi: true,
      todos: []
    })

  })

  it('should handle ADD_TODO', () => {

    /**
     * REDUCER ADD TODO
     */
     expect(
      reducer({
        todos: mockTodosJSON,
      }, actions.requestAddTodo())
    ).toEqual({
      loading: true,
      todos: mockTodosJSON
    })

    expect(
      reducer({
        todos: mockTodosJSON,
      }, actions.requestAddTodoSuccess(mockNewTodo))
    ).toEqual({
      todos: [mockNewTodo].concat(mockTodosJSON),
      loading: false
    })

    expect(
      reducer({
        todos: mockTodosJSON
      }, actions.requestAddTodoError(mockError))
    ).toEqual({
      loading: false,
      error: mockError,
      todos: mockTodosJSON
    })
  })

  it('should handle UPDATE_TODO', () => {

    /**
     * REDUCER UPDATE TODO
     */
     expect(
      reducer({
        todos: mockTodosJSON
      }, actions.updateTodo())
    ).toEqual({
      loading: true,
      todos: mockTodosJSON
    })

    expect(
      reducer({
        todos: mockTodosJSON
      }, actions.updateTodoSuccess({
        _id: 3,
        text: 'New Todo text 3',
        completed: true
      }))
    ).toEqual({
      todos: [
        { _id: 1, text: 'todo text 1', completed: false },
        { _id: 2, text: 'todo text 2', completed: true },
        { _id: 3, text: 'New Todo text 3', completed: true }
      ],
      loading: false,
    })

    expect(
      reducer({
        todos: mockTodosJSON
      }, actions.updateTodoError(mockError))
    ).toEqual({
      loading: false,
      error: mockError,
      todos: mockTodosJSON
    })
  })

  it('should handle DELETE_TODO', () => {
    
    /**
     * REDUCER DELETE TODO
     */
     expect(
      reducer({
        todos: mockTodosJSON
      }, actions.deleteTodo())
    ).toEqual({
      loading: true,
      todos: mockTodosJSON
    })

    expect(
      reducer({
        todos: mockTodosJSON
      },
        actions.deleteTodoSuccess(3)
      )
    ).toEqual({
      todos: [
        { _id: 1, text: 'todo text 1', completed: false },
        { _id: 2, text: 'todo text 2', completed: true }
      ],
      loading: false
    })

    expect(
      reducer({
        loading: false
      }, actions.deleteTodoError(mockError))
    ).toEqual({
      loading: false,
      error: mockError
    })
  })

  it('should handle REQUEST_COMPLETE_ALL_TODOS', () => {
    
    /**
     * REDUCER COMPLETE ALL TODOS
     */
     expect(
      reducer({
        todos: mockTodosJSON
      }, actions.requestCompleteAllTodos())
    ).toEqual({
      loading: true,
      todos: mockTodosJSON
    })

    expect(
      reducer({
        todos: mockTodosJSON
      }, actions.requestCompleteAllTodosSuccess([
        { _id: 1, text: 'todo text 1', completed: true },
        { _id: 3, text: 'todo text 3', completed: true }
      ]))
    ).toEqual({
      loading: false,
      todos: [
        { _id: 1, text: 'todo text 1', completed: true },
        { _id: 2, text: 'todo text 2', completed: true },
        { _id: 3, text: 'todo text 3', completed: true }
      ]
    })

    expect(
      reducer({
        todos: mockTodosJSON
      }, actions.requestCompleteAllTodosError(mockError))
    ).toEqual({
      loading: false,
      todos: mockTodosJSON,
      error: mockError
    })
  })

})