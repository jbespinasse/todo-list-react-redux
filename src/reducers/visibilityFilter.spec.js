import reducer from './visibilityFilter'
import * as actions from '../actions/todosAction'
import * as filters from '../constants/TodoFilters'

const initialVisibilityFilterState = filters.SHOW_ALL

describe('visibilityFilter reducer', () => {

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialVisibilityFilterState)
  })

  it('should handle SET_VISIBILITY_FILTER', () => {
    expect(
      reducer(undefined, actions.setVisibilityFilter(filters.SHOW_ALL))
    ).toEqual(filters.SHOW_ALL)
  })

})