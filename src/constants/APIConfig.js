const APIConfig = {
  "API_URI": "http://localhost:3001/api/",
  "HEADERS": new Headers({
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer VotreCléAPI",
  })
}

export default APIConfig