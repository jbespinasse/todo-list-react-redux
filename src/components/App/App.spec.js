import React from 'react'
import { createRenderer } from 'react-test-renderer/shallow'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import MainSection from '../../containers/MainSection'
import Loading from '../../containers/Loading'
import MissingApi from '../../containers/MissingApi'
import App from './index'

Enzyme.configure({ adapter: new Adapter() })

const setup = () => {
	const renderer = createRenderer()
	renderer.render(<App />)
	const output = renderer.getRenderOutput()
	return output
}

describe('components', () => {

	describe('MainSection', () => {
		it('should render', () => {
			const output = setup()
			const [, mainSection] = output.props.children
			expect(mainSection.type).toBe(MainSection)
		})
	})

	describe('Loading', () => {
		it('should render', () => {
			const output = setup()
			const [,, loading] = output.props.children
			expect(loading.type).toBe(Loading)
		})
	})

	describe('MissingApi', () => {
		it('should render', () => {
			const output = setup()
			const [,,, missingApi] = output.props.children
			expect(missingApi.type).toBe(MissingApi)
		})
	})
})
