import React, { Component } from 'react'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faTimes, faEdit } from '@fortawesome/free-solid-svg-icons'
import MainSection from '../../containers/MainSection'
import Loading from '../../containers/Loading'
import MissingApi from '../../containers/MissingApi'

library.add(fab, faTimes, faEdit)

export default class App extends Component {

	constructor(props) {
		super(props)
		this.state = { id: null }
	}

	render = () => {
		return (
			<div>
				<h1 className="display-4 text-center">Todos List</h1>
				<MainSection />
				<Loading />
				<MissingApi />
			</div>
		)
	}
}
