import React, { Component } from 'react'
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class ConfirmDialog extends Component {

	render () {
		const { item, confirmClicked, onClose, open, title, description } = this.props

		return (
			<Dialog
				open={open}
				onClose={() => onClose(false)}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				{title && <DialogTitle id="alert-dialog-title">{title}</DialogTitle>}
				{description && <DialogContent>
					<DialogContentText id="alert-dialog-description">
						{description || ''}
					</DialogContentText>
				</DialogContent>}
				<DialogActions>
					<Button onClick={() => onClose(false)} color="primary">
						Cancel
					</Button>
					<Button onClick={() => confirmClicked(item)} color="primary" autoFocus>
						Confirm
					</Button>
				</DialogActions>
			</Dialog>
		)
	}
}
