import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class TodoTextInput extends Component {

	static propTypes = {
		onSave: PropTypes.func.isRequired,
    cancelUpdate: PropTypes.func.isRequired,
		text: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
	}

  constructor(props) {
    super(props)
    this.state = { 
      text: this.props.text,
      id: this.props.id
    }
  }

  handleChange = (e) => {
    this.setState((state, props) => ({
      id: this.props.id,
      text: e.target.value.trim()
    }))
  }

  handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      this.handleSubmit()
    }
  }

  handleSubmit = () => {
    if (this.state.text) {
      this.props.onSave(this.props.id, this.state.text)
    }
  }

  cancelSubmit = () => {
    this.props.cancelUpdate()
  }

	render () {
    return (
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder={this.props.text}
          aria-label={this.props.text}
          aria-describedby="basic-addon2"
          autoFocus={true}
          onChange={this.handleChange}
          onKeyDown={this.handleKeyDown}
        />
        <div className="input-group-append">
        <button
          className="btn btn-outline-secondary" 
          type="button"
          onClick={this.cancelSubmit}>
            Cancel
          </button>
          <button
            className="btn btn-outline-secondary"
            type="button"
            onClick={this.handleSubmit}>
              Save
            </button>
        </div>
      </div>
    )
	}
}
