import React, { Component } from 'react'
import TodoItem from '../../containers/TodoItem'
import './TodoList.scss'

export default class TodoList extends Component {

	render = () => {
		return (
			<div className="row">
				<div className="col-12">
					<ul className="list-group list-group-flush">
						{this.props.filteredTodos.map((todo) => 
							<TodoItem key={todo._id} todo={todo} />
						)}
					</ul>
				</div>
			</div>
		)
	}
}