import React, { Component } from 'react'

export default class CompleteTodos extends Component {

  render = () => {
    const { completedCount, uncompletedCount, todosCount, fetchCompleteAllTodos } = this.props

    return (
      <div className="row">
        <div className="col-12">
          <ul className="list-group list-group-flush">
            <li className="list-group-item list-group-item-dark">
            {
                !!todosCount
                && <div>
                    <span>
                    <input
                      className="toggle-all"
                      type="checkbox"
                      checked={completedCount === todosCount}
                      onChange={e => {}}
                      onClick={fetchCompleteAllTodos}
                    /> Complete all todos
                  </span>
                  <span className="float-right font-weight-bold">/ Uncompleted: {uncompletedCount}</span>
                  <span className="float-right font-weight-bold mr-2">Completed: {completedCount}</span>
                </div>
            }
            </li>
          </ul>
        </div>
      </div>
    )
  }
}