import React, { Component } from 'react'
import classnames from 'classnames'
import './MissingApi.scss'

export default class MissingApi extends Component {

  constructor(props) {
    super(props)
  }

  missingApiChanged = (missingApiChanged) => {
    return missingApiChanged
  }

	render () {
    const { missingApi } = this.props

		const element = (
			<div className="missingapi-container">
        {/* Mettre un "close" pour fermer cet écran (via le store)*/}
        <h1>Impossible de requêter le serveur</h1>
        <p>
          Avez-vous installé l'<a href="https://gitlab.com/jbespinasse/node-mongo-api" target="_blank" rel="noreferrer">API</a> backend ? Pour se faire, suivez la procédure ci-dessous.<br />
          - Tout d'abord, installez l'API depuis gitlab :
        </p>
        <div className="box-shadow">
          <p>git clone git@gitlab.com:jbespinasse/node-mongo-api.git</p>
        </div>
        <p className="mt-2">
        - Ensuite, déplacez vous dans le dossier de l'application et installer les paquets npm :
        </p>
        <div className="box-shadow">
          <p>cd node-mongo-api &#38;&#38; npm install</p>
        </div>
        <p className="mt-2">
        - Puis démarrez le serveur de l'application :
        </p>
        <div className="box-shadow">
          <p>npm run start:dev</p>
        </div>
        <div className="alert alert-dark mt-3" role="alert">
          Attention ! Vous devez avoir installé mongodb sur votre post pour faire tourner cette API.<br />
          - Manuel d'installation : <a href="https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/" target="_blank" rel="noreferrer">ici</a><br />
          - Page de téléchargement : <a href="https://www.mongodb.com/try/download/community?tck=docs_server" target="_blank" rel="noreferrer">ici</a>
        </div>
        <p className="mt-3">
          Le serveur devrait démarrer sur le port 3000 (http://localhost:3000).<br />
          Si vous souhaitez changer l'url de l'API au sein de react allez 
          dans <code>src/constants/APIConfig.js</code> et modifiez API_URI en conséquence.
        </p>
        <div className="box-shadow">
          <p>&gt;&gt; "API_URI": "http://localhost:3000/api/"</p>
        </div>
      </div>
		)

		return (
			<div className={classnames({
        'missingapi-screen': true,
        active: this.missingApiChanged(missingApi)
      })}>
				{element}
			</div>
		)
	}
}
