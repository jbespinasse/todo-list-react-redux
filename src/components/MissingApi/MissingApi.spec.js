import { create } from 'react-test-renderer'
import MissingApi from './index'

const getComponent = (props = { missingApi: true }) => {
  return create(<MissingApi {...props} />)
}

describe('Test MissingApi', () => {
  
  it('active class should not appear', () => {
    const root = getComponent({ missingApi: false }).root
    const element = root.findByType('div')
    expect(element.props.className.includes('active')).toBeFalsy()
  })
  
  it('active class should appear', () => {
    const root = getComponent().root
    const element = root.findByType('div')
    expect(element.props.className.includes('active')).toBeTruthy()
  })
})