import { Component } from "react";

class MainSection extends Component {

  constructor(props) {
    super(props)
    this.shouldComponentRender = this.shouldComponentRender.bind(this)
  }

  componentWillMount() {
    const { fetchTodos } = this.props
    fetchTodos()
  }

  shouldComponentRender() {
    const { loading } = this.props
    if (loading === false) return false
    return true
  }

  render() {
    const { todosCount, completedCount, actions } = this.props
    if (!this.shouldComponentRender()) return ''
    return (
      <section className="main">
        {
          !!todosCount
            && <span>
              <input
                className="toggle-all"
                type="checkbox"
                defaultChecked={completedCount === todosCount}
              />
              <label onClick={actions.completeAllTodos} />
            </span>
        }
        <VisibleTodoList />
        {/*
          !!todosCount
            && <Footer
              completedCount={completedCount}
              activeCount={todosCount - completedCount}
              onClearCompleted={actions.clearCompleted}
            />
        */}
      </section>
    )
  }

}