import React, { Component } from 'react'
import VisibleTodoList from '../../containers/VisibleTodoList'
import AddTodo from '../../containers/AddTodo'
import CompleteTodos from '../../containers/CompleteTodos'
import FilterTodos from '../../containers/FilterTodos'

export default class MainSection extends Component {

	render = () => {
		const { completedCount, uncompletedCount } = this.props
		
		return (
			<section className="main">
				<CompleteTodos
					completedCount={completedCount}
					uncompletedCount={uncompletedCount}
				/>
				<FilterTodos />
				<AddTodo />
				<VisibleTodoList />
			</section>
		)
	}
}
