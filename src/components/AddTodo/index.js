import React, { Component } from 'react'
import './AddTodo.scss'

export default class AddTodo extends Component {

  constructor(props) {
    super(props)
    this.state = { todo: { text: '' } }
  }

  handleChange = (e) => {
    this.setState((state, props) => ({
      ...this.state,
      todo: { text: e.target.value.trim() }
    }))
  }

  handleSubmit = (e) => {
    e.preventDefault()
    if (this.state.todo.text) {
      this.props.saveTodo(this.state.todo)
      this.setState({ ...this.state, todo: { text: '' } })
    }
  }

  render = () => {
    const { todoIdChanged } = this.props
    return (
      <div className="row add-todo">
        <div className="col-12">
          <form onSubmit={this.handleSubmit}>
            <div className="input-group mb-3">
              <input
                type="text"
                className="form-control" 
                placeholder="Type your new todo here"
                aria-label="Type your new todo here"
                aria-describedby="basic-addon2"
                onChange={this.handleChange}
                value={this.state.todo.text}
                onFocus={() => todoIdChanged(null)}
                required
              />
              <div className="input-group-append">
                <button 
                  className="btn btn-outline-secondary"
                  type="submit">
                    Add
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}