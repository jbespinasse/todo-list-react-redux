import { create } from 'react-test-renderer'
import AddTodo from './index'

const props = {
  saveTodo: jest.fn(),
  todoIdChanged: jest.fn()
}

const getComponent = () => {
  return create(<AddTodo {...props} />)
}

describe('Test Add Todo', () => {

  // beforeEach(() => {})

  it('it shows the expected text at start (empty text)', () => {
    const component = getComponent().getInstance()
    
    expect(component.state.todo.text).toBe('')
  })

  it('it shows the expected text when changed', () => {
    const component = getComponent().getInstance()

    component.handleChange({ target: { value: 'New todo' } })
    expect(component.state.todo.text).toBe('New todo');
  })

  it('it shows the expected action on todo input focused', () => {
    const component = getComponent()
    const input = component.root.findByType('input')

    input.props.onFocus()
    expect(props.todoIdChanged).toHaveBeenCalled()
    expect(props.todoIdChanged).toHaveBeenCalledTimes(1)
  })

  it('State must be changed on sumblit form', () => {
    const component = getComponent()
    const form = component.root.findByType('form')
    const input = component.root.findByType('input')
    const todoText = 'New todo'
    
    form.props.onSubmit({ preventDefault: () => {} })
    expect(props.saveTodo).not.toHaveBeenCalled()

    input.props.onChange({ target: { value: todoText } })
    expect(component.getInstance().state.todo.text).toBe(todoText)
    expect(input.props.value).toEqual(todoText)

    form.props.onSubmit({ preventDefault: () => {} })
    expect(component.getInstance().state.todo.text).toBe('')
    expect(props.saveTodo).toHaveBeenCalled()
    expect(props.saveTodo).toHaveBeenCalledTimes(1)
  })

})