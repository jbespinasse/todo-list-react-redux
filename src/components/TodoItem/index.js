import React, { Component } from 'react'
import classnames from 'classnames'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './TodoItem.scss'
import TodoTextInput from '../TodoTextInput'
import ConfirmDialog from '../../containers/ConfirmDialog'

export default class TodoItem extends Component {

  constructor(props) {
    super(props)
		this.state = { editing: false, openConfirmDelete: false }
  }

	handleEditClicked = (id) => {
		this.setState((state, props) => ({ ...state, editing: true }))
		this.props.todoIdChanged(id)
	}

	handleUpdate = (id, text) => {
		this.setState((state, props) => ({ ...state, editing: false }))
		this.props.updateTodo({ _id: id, text: text })
	}

	cancelUpdate = () => {
		this.setState((state, props) => ({ ...state, editing: false }))
	}

	openConfirmDelete = (open) => {
		this.setState((state, props) => ({ ...state, openConfirmDelete: open }))
	}

	render () {
		const { todo, updateTodo, deleteTodo, currentTodoId } = this.props

		let element
		if (this.state.editing && currentTodoId === todo._id) {
			element = (
				<TodoTextInput
					text={todo.text}
					id={todo._id}
					onSave={(id, text) => this.handleUpdate(id, text)}
					cancelUpdate={this.cancelUpdate}
				/>
			)
		} else {
			element = (
				<div className="todo-item">
					<input
						type="checkbox"
						aria-label="Checkbox for following text input"
						checked={todo.completed}
						onChange={() => updateTodo({...todo, completed: !todo.completed})}
					/>
					{todo.completed ? <del>{todo.text}</del> : todo.text}
					<FontAwesomeIcon icon="times" onClick={() => this.openConfirmDelete(true)} />
					<FontAwesomeIcon icon="edit" onClick={() => this.handleEditClicked(todo._id)} />
					<ConfirmDialog 
						item={todo._id}
						confirmClicked={deleteTodo}
						onClose={this.openConfirmDelete}
						open={this.state.openConfirmDelete}
						title="Confirm Action"
						description="Please, confirm your deletion"
					/>
				</div>
			)
		}

		return (
			<li className={classnames({
				completed: todo.completed,
				editing: this.state.editing,
				"list-group-item": true
			})}>
				{element}
			</li>
		)
	}
}
