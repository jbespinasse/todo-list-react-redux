import React, { Component } from 'react'
import './FilterTodos.scss'
import * as filterActions from '../../constants/TodoFilters'

export default class FilterTodos extends Component {

  filterChanged = (e) => {
    const { filterChanged } = this.props
    filterChanged(e.target.value)
  }

  render = () => {

    return (
      <div className="row filter-todos mt-3">
        <div className="col-4">
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="inlineRadioOptions"
              id="inlineRadio1"
              value={filterActions.SHOW_ALL}
              onChange={this.filterChanged}
              defaultChecked={true}
            />
            <label className="form-check-label" htmlFor="inlineRadio1">Show all</label>
          </div>
        </div>
        <div className="col-4">
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="inlineRadioOptions"
              id="inlineRadio2"
              value={filterActions.SHOW_COMPLETED}
              onChange={this.filterChanged}
            />
            <label className="form-check-label" htmlFor="inlineRadio2">Show completed</label>
          </div>
        </div>
        <div className="col-4">
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="inlineRadioOptions"
              id="inlineRadio3"
              value={filterActions.SHOW_ACTIVE}
              onChange={this.filterChanged}
            />
            <label className="form-check-label" htmlFor="inlineRadio3">Show active</label>
          </div>
        </div>
      </div>
    )
  }
}