import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import * as TodoActions from '../../actions/todosAction'
import TodoList from '../../components/TodoList'
import { getVisibleTodos } from '../../selectors'

TodoList.propTypes = {
	filteredTodos: PropTypes.arrayOf(PropTypes.shape({
		_id: PropTypes.string.isRequired,
		completed: PropTypes.bool.isRequired,
		text: PropTypes.string.isRequired,
		dateAdd: PropTypes.string,
		dateUpd: PropTypes.string,
	}).isRequired).isRequired,
	actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	filteredTodos: getVisibleTodos(state)
})

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(TodoActions, dispatch)
})

const VisibleTodoList = connect(
	mapStateToProps,
	mapDispatchToProps
)(TodoList)

export default VisibleTodoList