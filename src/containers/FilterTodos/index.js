import { connect } from 'react-redux'
import { setVisibilityFilter } from '../../actions/todosAction'
import FilterTodos from '../../components/FilterTodos';

const mapDispatchToProps = dispatch => ({
  filterChanged: (filter) => { dispatch(setVisibilityFilter(filter)) },
});

export default connect(null, mapDispatchToProps)(FilterTodos)
