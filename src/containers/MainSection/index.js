import { connect } from 'react-redux'
import MainSection from '../../components/MainSection'
import { getCompletedTodoCount, getUncompletedTodoCount } from '../../selectors'

const mapStateToProps = (state) => ({
	completedCount: getCompletedTodoCount(state),
	uncompletedCount: getUncompletedTodoCount(state)
})

export default connect(mapStateToProps)(MainSection)