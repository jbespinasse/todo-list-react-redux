import { connect } from 'react-redux'
import Loading from '../../components/Loading'

const mapStateToProps = (state) => ({
  loading: state.todos.loading
})

export default connect(mapStateToProps, null)(Loading)
