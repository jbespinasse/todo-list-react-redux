import { connect } from 'react-redux'
import MissingApi from '../../components/MissingApi'

const mapStateToProps = (state) => ({
  missingApi: state.todos.missingApi
})

export default connect(mapStateToProps, null)(MissingApi)
