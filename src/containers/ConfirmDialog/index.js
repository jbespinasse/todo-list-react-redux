import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ConfirmDialog from '../../components/ConfirmDialog'

ConfirmDialog.propTypes = {
  item: PropTypes.any.isRequired,
  confirmClicked: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool,
  description: PropTypes.string,
  title: PropTypes.string
}

export default connect()(ConfirmDialog)
