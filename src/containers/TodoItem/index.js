import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { fetchDeleteTodo, fetchUpdateTodo, todoIdChanged } from '../../actions/todosAction'
import TodoItem from '../../components/TodoItem'

TodoItem.propTypes = {
  deleteTodo: PropTypes.func.isRequired,
  updateTodo: PropTypes.func.isRequired,
  todo: PropTypes.object.isRequired,
  currentEditingTodoId: PropTypes.string
}

const mapStateToProps = (state) => ({
	currentTodoId: state.todo.id,
})

const mapDispatchToProps = dispatch => ({
  deleteTodo: (id) => { dispatch(fetchDeleteTodo(id)) },
  updateTodo: (todo) => { dispatch(fetchUpdateTodo(todo)) },
  todoIdChanged: (id) => { dispatch(todoIdChanged(id)) }
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem)
