import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { fetchAddTodo, todoIdChanged } from '../../actions/todosAction'
import AddTodo from '../../components/AddTodo'

AddTodo.propTypes = {
  saveTodo: PropTypes.func.isRequired,
  todoIdChanged: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => ({
  saveTodo: (todo) => { dispatch(fetchAddTodo(todo)) },
  todoIdChanged: (id) => { dispatch(todoIdChanged(id)) }
});

export default connect(null, mapDispatchToProps)(AddTodo)
