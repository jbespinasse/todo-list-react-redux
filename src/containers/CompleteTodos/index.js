import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { fetchCompleteAllTodos } from '../../actions/todosAction'
import CompleteTodos from '../../components/CompleteTodos'
import { getCompletedTodoCount, getUncompletedTodoCount } from '../../selectors'

CompleteTodos.propTypes = {
  todosCount: PropTypes.number.isRequired,
  fetchCompleteAllTodos: PropTypes.func.isRequired,
  completedCount: PropTypes.number.isRequired,
  uncompletedCount: PropTypes.number.isRequired
}

const mapStateToProps = (state) => ({
	todosCount: state.todos.todos.length,
	completedCount: getCompletedTodoCount(state),
	uncompletedCount: getUncompletedTodoCount(state)
})

const mapDispatchToProps = dispatch => ({
  fetchCompleteAllTodos: () => { dispatch(fetchCompleteAllTodos()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(CompleteTodos)
