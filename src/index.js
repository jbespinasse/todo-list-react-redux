import React from 'react';
import './index.scss';
import App from './components/App';
import { render } from 'react-dom'
import { applyMiddleware, compose, createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import thunk from 'redux-thunk'
import { fetchTodos } from './actions/todosAction';

const composeEnchancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  composeEnchancer(applyMiddleware(thunk))
)

store.dispatch(fetchTodos());

render(
  <Provider store={store}>
		<div className="container-fluid">
      <App />
    </div>
	</Provider>,
  document.getElementById('root')
)